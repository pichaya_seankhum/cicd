const express = require('express')
const app = express()
const port = 3000

app.use(express.static('public'))

app.get('/', (req, res) => {
  res.send('Hello World! on feature 1')
})

app.get('/h', (req, res) => {
  res.send('สวัสดีคร๊าบบบ on dev')
})

app.get('/h3', (req, res) => {
  res.send('สวัสดีคร๊าบบบ on feature-1')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})